import { Grid, Typography } from "@mui/material"

function ComponentLogo (){
    return(
    <Grid lg={6} item>
        <Typography style={{ fontSize: "30px", fontWeight: 900, paddingLeft: "100px" }}>Devcamp</Typography>
    </Grid>
    )
}

export default ComponentLogo