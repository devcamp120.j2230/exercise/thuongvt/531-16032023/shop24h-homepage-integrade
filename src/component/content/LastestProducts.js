import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from "@mui/material"
import { Container } from "@mui/system"
import { useEffect, useState } from "react";

function LastestProducts() {

    const [product, setProduct] = useState([]);

    const fetchAPI = async (url, requestOptions) => {
        let res = await fetch(url, requestOptions);
        let data = await res.json()
        return data
    }
    const getAllHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        var url = "http://localhost:8000/products"
        fetchAPI(url, requestOptions)
            .then((response) => {
                setProduct(response.Data)
                console.log(response.Data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    useEffect(() => {
        getAllHandler()
    },[]);

    return (
        <>
            <Container className="text-center">
                <Grid className="latest-product">
                    <Typography style={{ fontSize: "25px", fontWeight: 500, marginTop:"30px"}}>LATEST PRODUCT</Typography>
                </Grid>
            </Container>
            <Container>
                <Grid item xs={3} >
                    <Grid item xs={12} sm={12} md={12} lg={3} style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                        {product.slice(0, 8).map((element, index) => {
                            return <Card key={index} sx={{ maxWidth: 250 }} style={{ marginRight: "30px", marginTop: "15px", marginBottom: "15px" }}>
                                <CardActionArea>
                                    <CardMedia component="img"
                                        image={require("../../assetment/image/" + element.mageUrl)}
                                        height="250px"
                                        width="250px"
                                        style={{textAlign:"center"}}>
                                    </CardMedia>
                                </CardActionArea>
                                <CardContent style={{textAlign:"center"}}>
                                    <Typography style={{ marginTop: "10px", fontSize: "20px" }}>
                                        {element.Name}
                                    </Typography>
                                    <Typography >
                                        <del>{element.PromotionPrice}</del>{" "}<span className="text-danger fw-bold" style={{ fontSize: "20px" }}>{element.BuyPrice}</span>
                                    </Typography>
                                </CardContent>
                            </Card>
                        })}
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}
export default LastestProducts