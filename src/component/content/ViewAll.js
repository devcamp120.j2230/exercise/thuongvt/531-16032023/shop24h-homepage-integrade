import { Button, Container } from "@mui/material"

function ViewAll() {
    return (
        <>        
            <Container className="text-center">
                <Button variant="contained" style={{ marginTop: "15px", marginBottom: "15px" }} href="/products">WELL ALL</Button>
            </Container>
        </>
    )
}

export default ViewAll