import { Container } from "@mui/material"
import ContentCarousel from "./Carousel"
import LastestProducts from "./LastestProducts"
import ViewAll from "./ViewAll"



function Content (){
    return(
      <Container>
        <ContentCarousel></ContentCarousel>
        <LastestProducts></LastestProducts>
        <ViewAll></ViewAll>
      </Container>
    )
}

export default Content