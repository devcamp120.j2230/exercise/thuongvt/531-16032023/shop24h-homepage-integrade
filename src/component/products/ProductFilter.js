import Checkbox from '@mui/material/Checkbox';
import { FormControlLabel, FormGroup, Grid, Typography } from "@mui/material"
import Rating from '@mui/material/Rating';
import Stack from '@mui/material/Stack';
import { useState } from "react";




function ProductFilter() {
    // xử lý sự kiện filter pice
    const [checkedOnSale, setCheckedOnSale] = useState(false);
    const [checkedInStock, setCheckedInStock] = useState(false);
    const [checkedFeature, setCheckedFeature] = useState(false)

    const handleChangeOnSale = (event) => {
        setCheckedOnSale(event.target.checked);
    };
    const handleChangeInStock = (event) => {
        setCheckedInStock(event.target.checked);
    };

    const handleChangeFeature = (event) => {
        setCheckedFeature(event.target.checked);
    };
    //Xử lý sư kiện filter brand

    return (
        <>
            <Grid item lg={10}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Categorles</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-3">Wireless</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-3">In-ear headphone</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-3">Over-ear headphone</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-3">Sport headphone</Typography>
            </Grid>
            <Grid item lg={10} mt={4}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Pice</Typography>
                <FormGroup>
                    <FormControlLabel control={<Checkbox
                        checked={checkedOnSale}
                        onChange={handleChangeOnSale}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="On Sale" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedInStock}
                        onChange={handleChangeInStock}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="In Stock" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedFeature}
                        onChange={handleChangeFeature}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Featured" />
                </FormGroup>
            </Grid>
            <Grid item lg={10} mt={4}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Brand</Typography>
                <FormGroup>
                    <FormControlLabel control={<Checkbox
                        checked={checkedOnSale}
                        onChange={handleChangeOnSale}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="JBL" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedInStock}
                        onChange={handleChangeInStock}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Beat" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedFeature}
                        onChange={handleChangeFeature}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Logitech" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedFeature}
                        onChange={handleChangeFeature}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Sam Sung " />
                    <FormControlLabel control={<Checkbox
                        checked={checkedFeature}
                        onChange={handleChangeFeature}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Sony" />
                </FormGroup>
            </Grid>
            <Grid item lg={10} mt={4}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Colors</Typography>
                <FormGroup>
                    <FormControlLabel control={<Checkbox
                        checked={checkedOnSale}
                        onChange={handleChangeOnSale}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Red" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedInStock}
                        onChange={handleChangeInStock}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="BLue" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedFeature}
                        onChange={handleChangeFeature}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="White" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedFeature}
                        onChange={handleChangeFeature}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Pink" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedFeature}
                        onChange={handleChangeFeature}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Yellow" />
                </FormGroup>
            </Grid>
            <Grid item lg={10} mt={4}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Rating</Typography>
                <Stack spacing={1} mt={3}>
                    <Rating name="size-medium" defaultValue={3} />
                </Stack>
            </Grid>
        </>
    )
}
export default ProductFilter