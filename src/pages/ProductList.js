import { Box, Container, Grid} from "@mui/material"
import BreadCrumb from "../component/breadCrumb/BreadCrumb"
import Footer from "../component/footer/footer"
import Header from "../component/header/header"
import ProductCart from "../component/products/ProductCart";
import ProductFilter from "../component/products/ProductFilter";

function ProductList() {

    return (
        <>
            <Header></Header>
            <Container>
                <Grid item margin={3}>
                    <BreadCrumb></BreadCrumb>
                </Grid>
                <Grid item>
                    <Box>
                        <Grid container>
                            <Grid item xs={2}>
                                <ProductFilter></ProductFilter>
                            </Grid>
                            <Grid item xs={10} >
                                <ProductCart></ProductCart>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
            </Container>
            <Footer></Footer>
        </>
    )
}
export default ProductList