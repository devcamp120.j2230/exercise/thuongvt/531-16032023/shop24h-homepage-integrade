import Content from "../component/content/content";
import Footer from "../component/footer/footer";
import Header from "../component/header/header";

function HomePage() {
    return (
        <>
            <Header></Header>
            <Content></Content>
            <Footer></Footer>
        </>
    )
}
export default HomePage