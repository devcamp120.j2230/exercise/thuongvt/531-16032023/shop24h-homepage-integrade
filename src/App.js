import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Routes } from "react-router-dom";
import './App.css';
import HomePage from "./pages/homePage";
import RouteList from "./router";
function App() {
  return (
    <>
    <Routes>
      {RouteList.map((router,index)=>{
        if(router.path){
          return <Route key={index} path={router.path} element={router.element}>
          </Route>
        }
        else {
          return null
        }
      })}
      <Route path="*"element={<HomePage></HomePage>}></Route>
    </Routes>
    </>
  );
}

export default App;
